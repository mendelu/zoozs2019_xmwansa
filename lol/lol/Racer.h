//
// Created by xmwansa on 10.12.2019.
//

#ifndef MWANSA_TEST_RACER_H
#define MWANSA_TEST_RACER_H

#include <iostream>
using namespace std;

class Racer {
    string m_name;
    int m_age;
    float speed;

public:
    Racer(string name, int age);
    float getRacerSpeed();
    void getTrolley(string name);
    float getRacerSpeed(string name, float speed);
};


#endif //MWANSA_TEST_RACER_H
