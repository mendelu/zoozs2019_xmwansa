//
// Created by xmwansa on 10.12.2019.
//

#ifndef MWANSA_TEST_TROLLEY_H
#define MWANSA_TEST_TROLLEY_H

#include "Racer.h"

class Trolley {
public:
    virtual Racer* getSpeed(float speed) = 0;
    virtual  ~Trolley(){};
};


#endif //MWANSA_TEST_TROLLEY_H
