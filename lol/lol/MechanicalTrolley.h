//
// Created by xmwansa on 10.12.2019.
//

#ifndef MWANSA_TEST_MECHANICALTROLLEY_H
#define MWANSA_TEST_MECHANICALTROLLEY_H

#include "Trolley.h"

class MechanicalTrolley {
bool m_sport;
float m_speed = 0;
float m_weight = 0;

public:
    MechanicalTrolley(float speed, float weight, bool sport);
float setSpeed();
};


#endif //MWANSA_TEST_MECHANICALTROLLEY_H
