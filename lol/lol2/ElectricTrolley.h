//
// Created by xmwansa on 10.12.2019.
//

#ifndef MWANSA_TEST_ELECTRICTROLLEY_H
#define MWANSA_TEST_ELECTRICTROLLEY_H

#include "Trolley.h"

class ElectricTrolley: public Trolley {
float m_speed;
float m_weight;
float m_accelaration;

public:
    ElectricTrolley(float speed, float weight, float accelaration);
    float getSpeed();
    float getWeight();
    float getAccelaration();


};


#endif //MWANSA_TEST_ELECTRICTROLLEY_H
