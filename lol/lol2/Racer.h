//
// Created by xmwansa on 10.12.2019.
//

#ifndef MWANSA_TEST_RACER_H
#define MWANSA_TEST_RACER_H

#include "Trolley.h"

#include <iostream>
using namespace std;

class Racer {
    string m_name;
    int m_age;
    float m_speed;

public:
    Racer(string name, int age, Trolley type);
    Racer* createRacer(string name, Trolley type);
    float getRacerSpeed();
    void getTrolley(string name);
    float getRacerSpeed(string name, float speed);
    string getName();
    int getAge();
    float getSpeed();

};


#endif //MWANSA_TEST_RACER_H
