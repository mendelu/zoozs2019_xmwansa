//
// Created by xmwansa on 10.12.2019.
//

#include "ElectricTrolley.h"


ElectricTrolley::ElectricTrolley(float speed, float weight, float accelaration) {
    m_speed = speed;
    m_weight = weight;
    m_accelaration = accelaration;
}
float ElectricTrolley::getSpeed() {
    return (m_accelaration * m_speed) - (m_weight / 10);
}
Racer* Trolley::getSpeed(float speed) {
    return ElectricTrolley::getSpeed();
}