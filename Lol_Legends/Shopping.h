//
// Created by Calvin on 13.12.2019.
//

#ifndef PROJECT_SHOPPING_H
#define PROJECT_SHOPPING_H

#include "Player.h"
#include "Company.h"


class Shopping {
public:
    void PurchaseItem(Company& comp);
    void sellItem(Player& player);

    Shopping();
    ~Shopping();
};


#endif //PROJECT_SHOPPING_H
