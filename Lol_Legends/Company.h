//
// Created by Calvin on 13.12.2019.
//

#ifndef PROJECT_COMPANY_H
#define PROJECT_COMPANY_H

#include <vector>
//#include "Machine.h"
#include "Resources.h"
#include "Product.h"

using namespace std;

class Company {

    float m_capital = 1000000;

public:

    //vector<Machine> m_Machines;
    int m_MachineHoursAvailable;
    int m_MachineHoursRemaining;
    vector<Resources> m_Resources;
    int m_ResourceCount;
    vector<Product> m_Products;
    int m_ProductCount;
    int m_Cash;


};


#endif //PROJECT_COMPANY_H
