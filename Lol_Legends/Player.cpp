//
// Created by Calvin on 13.12.2019.
//

#include "Player.h"


string Player::getName() {
    return m_name;
}
int Player::getMoney() {
    return m_money;
}
string Player::addItem(string item, int itemPrice) {

   if (isInventoryFull())
   {
       cout<<"Inventory is full. ";
   }
   else{
       if(m_money >= itemPrice)
       {
           m_money -= itemPrice;
          numbOfItems++;
           // inventoryCapacity();
           cout<< "You have purchased" << item << "."<< endl;
           inventory.push_back(item);
           return item;
       }
       else{
           cout <<"You cannot afford this item. "<<endl;
       }
   }
}
void Player::sellItem(int itemNum, int itemPrice) {
    char response;
    cout<< "Are you sure that you want to sell: "<< inventory[itemNum]<< "? 'y'- Yes. 'n'- No."<<endl;
    cin >> response;
    switch (response){
        case 'y':
            numbOfItems--;
            m_money+= itemPrice;
            inventory.erase(inventory.begin()+ itemNum);
            break;
        case 'n':
            cout<<"that is ok. "<< endl;
            break;
        default:
            cout<< "Please enter correct data. "<< endl;
    }
}

bool Player::isInventoryFull()
{
    return numbOfItems >= m_maxNumbItems;
}
/*region test
bool Player::isInventoryFull() {
    if(Player::getNumbOfItems() < m_maxNumbItems)
    {
        return false;
    }
    else{
        return true;
    }
}
*/

int Player::inventoryCapacity() {
    return inventory.size();
}
void Player::printInfo() {
    int itemNumb=0;
    for  (int i =0; i<inventory.size(); i++){
        itemNumb++;
        cout<< itemNumb<< ":"<< inventory[i]<< endl;
    }
}
int Player::getNumbOfItems() {
    return m_maxNumbItems;
}
//int Player::inventoryCapacity() {
   // return m_maxNumbItems;
//}
Player::Player() {}
Player::~Player() {}
