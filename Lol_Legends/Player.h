//
// Created by Calvin on 13.12.2019.
//

#ifndef PROJECT_PLAYER_H
#define PROJECT_PLAYER_H

#include <vector>
#include <iostream>
using namespace std;



class Player {

private:
    const int m_maxNumbItems = 5;

    int m_money = 50000, numbOfItems = 0;

    vector<string> inventory;

    string m_name = "Calvin Mwansa";

public:
    string getName();
    string addItem(string item, int itemPrice);
    void sellItem(int itemNum, int itemPrice);
    bool isInventoryFull();
    int inventoryCapacity();
    int getMoney();
    void printInfo();
    int getNumbOfItems();

    Player();

    ~Player();

};


#endif //PROJECT_PLAYER_H
