#include <iostream>
using namespace std;

class Developer{
private:
float m_baseSalary;
float m_bonus;
float m_workHours;
float m_bugDeduction;
int m_bugs;
string m_name;
public:
    Developer(string name, float salary){
        m_name = name;
        m_baseSalary = salary;
        m_bonus = 500;
        m_bugDeduction = 1000;
        m_workHours = 0.0;
        m_bugs = 0;
    }
    Developer(string name, float salary, float bonus, float deduction){
        m_name = name;
        m_baseSalary = salary;
        m_bonus = bonus;
        m_bugDeduction = deduction;
        m_workHours = 0.0;
        m_bugs = 0;
    }
    void setBaseSalary(float salary){
        if (salary > 10000){
            m_baseSalary = salary;
        }
        else{
            cout << "Minimal salary is 10001" << endl;
        }
    }
    void addBug(){
        m_bugs++;
    }
    float calculateSalary(){
        float result = 0;

        result += m_baseSalary;
        if (m_workHours > 40){
            result += (m_workHours - 40) * m_bonus;
        }
        result -= m_bugs * m_bugDeduction;
        return result;
    }
void resetMonth(){
        m_bugs = 0;
        m_workHours = 0;
    }
    void setWorkHours(float hours){
        m_workHours = hours;
    }
    void printInfo(){
        
    }

};
int main() {

    Developer *tomas;

    tomas = new Developer("Tomas", 20000, 800, 1200);

    tomas ->addBug();
    tomas ->addBug();
    tomas ->addBug();
    tomas ->setWorkHours(50);

    float  totalSalary = tomas->calculateSalary();
    cout << "Month 1: " << totalSalary << endl;

    // new month
    tomas->resetMonth();
    tomas ->setBaseSalary(25000);
    tomas ->addBug();
    tomas ->setWorkHours(50);
    return 0;
}