#include <iostream>
#include <array>
#include <vector>

using namespace std;

class Box{
private:
    int m_weight;
    string m_content;
    string m_owner;
public:
    Box(int weight, string content, string owner){
        m_weight = weight;
        m_content = content;
        m_owner = owner;
    }
   string getContent(){
       return m_content;
    }
    string getOwner(){
        return m_owner;
    }
};
class Floor{
private:
    string m_label;
    array<Box*, 10> m_position;

public:
    Floor(string label){
        m_label = label;
        for (int i = 0; i < m_position.size(); ++i){
            m_position.at(i) = nullptr;
        }
    }
    void  saveBox(int position, Box *box){
        if ((position >= 0) and (position <= 9)){
            if (m_position.at(position) == nullptr){
                m_position.at(position);
            }
            else{
                cout<<"There is a box at position " << position <<"! \n";
            }
        }
        else{
            cout<<"You are saving out of range :/";
        }
    }
    void  removeBox(int position){
        if ((position >= 0) and (position <= 9)){
            if (m_position.at(position) != nullptr){
                m_position.at(position) = nullptr;
            }
            else{
                cout<<"There is no box in position "<< position << "! \n";
            }
        }
    }
    void printInfo() {
        cout << endl <<"Floor state: " << endl;
        for(Box *currentPosition:m_position){
            if (currentPosition != nullptr){
                cout << currentPosition->getOwner() << ": "<< currentPosition->getContent() << endl;
            }
            else{
                cout << "Position is empty"<< endl;
            }
        }
    }
};
class Store{
    vector<Floor *> m_floors;
public:
    Store(){
        m_floors.push_back(new Floor("Floor n.0"));
    }
    void buildNewFloor(){
        m_floors.push_back(new Floor("Floor n. " + to_string(m_floors.size())));
    }
    void destroyLastFloor(){
        delete(m_floors.at(m_floors.size() -1));
        m_floors.pop_back();
    }
    void storeBox(int floor, int position, Box *box){
        m_floors.at(floor) ->saveBox(position, box);
    }
    void removeBox(int floor, int position){
        m_floors.at(floor) ->removeBox(position);
    }
    void printInfo(){
        for (auto *floor:m_floors){
            floor->printInfo();
        }
    }
    ~Store(){
        for (auto *floor:m_floors) {
            delete(floor);
        }
    }
};
int main() {
    auto *boxik = new Box(100, "Lentilky", "Tomas");
  Store *store = new Store();
  store->buildNewFloor();
    store->buildNewFloor();
    store->destroyLastFloor();
    store->storeBox(0,5, boxik);
    store->printInfo();
   /* auto floor1 = new Floor("Floor 1");
    floor1->saveBox(5, boxik);
*/

    return 0;
}