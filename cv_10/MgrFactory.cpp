//
// Created by Calvin on 03.12.2019.
//

#include "MgrFactory.h"


Student* MgrFactory::createStudent(string name) {
    return Student::createStudent(name, StudyType::Mgr);
}
Course* MgrFactory::createCourse(string name, int credits) {
    return new Course(name, credits, StudyType::Mgr);
}