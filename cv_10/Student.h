//
// Created by Calvin on 03.12.2019.
//

#ifndef CV_10_STUDENT_H
#define CV_10_STUDENT_H

#include "StudyType.h"
#include <iostream>

using namespace std;



class Student {
    string m_name;
    float m_scholarshipPerYear;
    int m_standardStudyLenght;
    bool m_mealDiscount;
    Student(string name, float scholarship, int studyLenght, bool meal);
public:
    static Student* createStudent(string name, StudyType);
    string getName();
    float getSholarshipPerYear();
    int getStandardStudyLenght();
    bool getmealDiscount();
};


#endif //CV_10_STUDENT_H
