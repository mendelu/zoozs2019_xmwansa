//
// Created by Calvin on 03.12.2019.
//

#ifndef CV_10_STUDYFACTORY_H
#define CV_10_STUDYFACTORY_H

#include "Student.h"
#include "Course.h"

class StudyFactory {
public:
    virtual Student* createStudent(string name) = 0;
    virtual ~StudyFactory(){}
    virtual Course* createCourse(string name, int credits) = 0;

};


#endif //CV_10_STUDYFACTORY_H
