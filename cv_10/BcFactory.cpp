//
// Created by Calvin on 03.12.2019.
//

#include "BcFactory.h"

Student* BcFactory::createStudent(string name) {
    return Student::createStudent(name, StudyType::Bc);
}
Course* BcFactory::createCourse(string name, int credits) {
    return new Course(name, credits, StudyType::Bc);
}