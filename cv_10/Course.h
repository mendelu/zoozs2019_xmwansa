//
// Created by Calvin on 03.12.2019.
//

#ifndef CV_10_COURSE_H
#define CV_10_COURSE_H

#include <iostream>
#include "StudyType.h"
using namespace std;

class Course {
string m_name;
int m_credits;
StudyType m_type;

public:
    Course(string name, int credits, StudyType type);
    string getName();
    int getCredits();
    StudyType getStudyType();
};


#endif //CV_10_COURSE_H
