//
// Created by Calvin on 03.12.2019.
//

#ifndef CV_10_BCFACTORY_H
#define CV_10_BCFACTORY_H
#include "StudyFactory.h"
#include <iostream>
using namespace std;

class BcFactory: public StudyFactory {
public:
    Student* createStudent(string name);
    Course* createCourse(string name, int credits);

};

#endif //CV_10_BCFACTORY_H
