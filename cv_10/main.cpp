#include <iostream>
#include "Student.h"
#include "StudyFactory.h"
#include "MgrFactory.h"
#include "BcFactory.h"
using namespace std;

int main() {

    /*Student *bc = new Student("Robert Novak", 1000, 3, true);
    Student *mgr = new Student("Filip Novak", 2000, 2, true);
    Student *phd = new Student("Petr Novak", 0, 3, false);
*/
   // Student *bc = Student::createStudent("Martin Novak", StudyType::Bc);
   // Student *mgr = Student::createStudent("Josef Prochazka", StudyType::Mgr);
   // Student *phd = Student::createStudent("Petr Holec", StudyType::Phd);

   StudyFactory *factory;

   int type;

   cout << "What kind of student do you want to be? Bc=1, Mgr=2:  \t";
   cin>> type;

   switch (type) {
       case 1:
           factory = new BcFactory();
           break;
       case 2:
           factory = new MgrFactory();
           break;
       default:
           cout << "Zadali jste spatne cislo!!";
           break;
   }

   auto* someStudent = factory->createStudent("Josef");
   auto* someCourse = factory->createCourse("ZOO", 6);
   cout << "Student scholarship: " << someStudent->getSholarshipPerYear() << endl;
   cout << "Course type: " << someCourse->getName() << endl;

    delete factory;

    return 0;
}