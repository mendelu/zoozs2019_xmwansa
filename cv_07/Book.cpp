//
// Created by xmwansa on 12.11.2019.
//

#include "Book.h"

int Book::s_instances = 1;

Book::Book(string author, string title) {
    m_autor = author;
    m_title = title;
    s_instances += 1;
    m_id = s_instances;
}

string Book::getAuthor() {
    return m_autor;
}

int Book::getId() {
    return m_id;
}

void Book::printInfo() {
    cout<<"Author: " << m_autor << ", Title: " << m_title << ", ID: " << m_title << endl;
}