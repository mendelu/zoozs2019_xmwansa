//
// Created by xmwansa on 12.11.2019.
//

#ifndef CV_07_BOOK_H
#define CV_07_BOOK_H
using namespace std;
#include <iostream>
class Book {
private:
    string m_autor;
string m_title;
int m_id;
static int s_instances;
public:
    Book(string author, string title);

    int getId();

    void printInfo();

    string getAuthor();

};


#endif //CV_07_BOOK_H
