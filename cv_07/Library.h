//
// Created by xmwansa on 12.11.2019.
//

#ifndef CV_07_LIBRARY_H
#define CV_07_LIBRARY_H
#include <iostream>
#include <vector>
#include "Book.h"

using namespace std;

class Library {
static vector<Book *> s_books;
Library() = default;

public:
    static void createBook(string author, string title);

    staitc void addBook(Book *new Book);

    static void removeBook(int id);

    static void printInfo();

    static vector<Book*> searchBooksByAuthor(string author);
};


#endif //CV_07_LIBRARY_H
