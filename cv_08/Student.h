//
// Created by Calvin on 19.11.2019.
//

#ifndef ZOO2_STUDENT_H
#define ZOO2_STUDENT_H

#include "Person.h"
class Student: public Person {
int m_semester;
float m_average;
public:
    Student(std::string name, std::string id, int semester, float average);
};


#endif //ZOO2_STUDENT_H
