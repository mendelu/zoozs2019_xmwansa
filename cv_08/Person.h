//
// Created by Calvin on 19.11.2019.
//

#ifndef ZOO2_PERSON_H
#define ZOO2_PERSON_H

#include <iostream>
using namespace std;

class Person {
protected:
    std::string m_name;
    std::string m_id;

public:
    Person(std::string name, std::string id);

    std::string getName();
    std::string getId();

    void setName(std::string name);

    void setId(std::string id);

    void printInfo();
};


#endif //ZOO2_PERSON_H
