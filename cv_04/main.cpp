#include <iostream>

using namespace std;

class Dragon {

    int m_health = 100;
    int m_damage;
    int m_armor;
private:
    void setDamage(int d) {
        if ((d > 0) && (d <= 100)) {
            m_damage = d;
        }
    }

    void setArmor(int a) {
        if ((a > 0) && (a < 200)) {
            m_armor = a;
        } else {
            // TODO
        }
    }

public:
    Dragon(int damage, int armor) {
        setDamage(damage);
        setArmor(armor);
        m_health = 100;
    }

    int getDamage() {
        return m_damage;
    }

    int getArmor() {
        return m_armor;
    }

    int getHealth() {
        return m_health;
    }
    void reduceHealth(int count) {
        m_health -= count;
    }
};

class Knight {
    int k_health = 100;
    int k_damage;
    int k_armor;
    string k_name;
private:
    void setDamageKnight(int d) {
        if ((d > 0) && (d <= 100)) {
            k_damage = d;
        } else {
            // TODO
        }
    }

    void setArmorKnight(int a) {
        if ((a > 0) && (a < 200)) {
            k_armor = a;
        } else {
            // TODO
        }
    }

    void setName(string name) {
        k_name = name;
    }

public:
    Knight(int damage, int armor, string name) {
        setDamageKnight(damage);
        setArmorKnight(armor);
        k_health = 100;
        setName(name);
    }

    string getName() {
        return k_name;
    }

    int getArmor() {
        return k_armor;
    }

    int getDamage() {
        return k_armor;
    }

    int getHealth() {
        return k_health;
    }

    void reduceHealth(int count) {
        k_health -= count;
    }

    void attack(Dragon *smoug) {

        int dragonPower = smoug->getDamage();
        if (dragonPower > k_armor) {
            k_health -= dragonPower - k_armor;
        }
        int dragonArmor = smoug->getArmor();
        if (dragonArmor < k_damage){
            smoug->reduceHealth(k_damage - dragonArmor);
        }

/*class ErrorLog{
    static ErrorLog * s_log;
            string m_errors;
        public:
    static ErrorLog * s_log;
        };
    }
    */
};


int main() {
    Dragon *Smoug= new Dragon(75, 69);
    Knight *knight= new Knight(83, 67, "Bilbo Baggins");

    knight->attack(Smoug);
    cout << knight->getName() << ": " << knight->getHealth() << endl;
    cout << "Smoug has " << Smoug->getHealth() << " health left" << endl;

    return 0;
}