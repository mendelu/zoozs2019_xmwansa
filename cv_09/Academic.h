//
// Created by xmwansa on 26.11.2019.
//

#ifndef CV_09_ACADEMIC_H
#define CV_09_ACADEMIC_H

#include "jobPosition.h"

class Academic: public jobPosition {

    float getSalary(int years, int education);
    int getRestHoliday(int usedHoliday);
};


#endif //CV_09_ACADEMIC_H
