#include <iostream>
using namespace std;

#include "Employee.h"
int main() {
    Employee* e1 = new Employee(5, 4, PositionType::ACADEMIC);
    e1->printInfo();
    e1->changeJobPosition(PositionType::ENGINEER);
    e1->printInfo();

    delete e1;

    return 0;
}