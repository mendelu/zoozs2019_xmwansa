//
// Created by xmwansa on 26.11.2019.
//

#include "Academic.h"

float Academic::getSalary(int years, int education) {

    const int baseSalary = 40000;
    const int educationalBonus = 5000;

    return baseSalary + education * educationalBonus;
}

int Academic::getRestHoliday(int usedHoliday) {
    const int maxHoliday = 50;

    return  maxHoliday - usedHoliday;
}