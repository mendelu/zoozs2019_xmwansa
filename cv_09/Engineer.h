//
// Created by xmwansa on 26.11.2019.
//

#ifndef CV_09_ENGINEER_H
#define CV_09_ENGINEER_H

#include "jobPosition.h"

class Engineer: public jobPosition {

    float getSalary(int years, int education);
    int getRestHoliday(int usedHoliday);

};


#endif //CV_09_ENGINEER_H
