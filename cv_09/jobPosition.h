//
// Created by xmwansa on 26.11.2019.
//

#ifndef CV_09_JOBPOSITION_H
#define CV_09_JOBPOSITION_H


class jobPosition {

public:
    virtual float getSalary(int years, int education) = 0;
    virtual int getRestHoliday(int usedHoliday) = 0;

};


#endif //CV_09_JOBPOSITION_H
