//
// Created by xmwansa on 26.11.2019.
//

#ifndef CV_09_EMPLOYEE_H
#define CV_09_EMPLOYEE_H

#include "jobPosition.h"
#include "Academic.h"
#include "Engineer.h"
using namespace std;
enum class PositionType{
    ACADEMIC, ENGINEER
};

class Employee {
private:
    int m_usedHoliday;
    int m_years;
    int m_education;
    jobPosition *m_position;

public:

    Employee(int years, int education, PositionType positionType);

    //~Employee();

    void changeJobPosition(PositionType newPosition);

    void addNewHoliday(int newHoliday);

    void printInfo();



};


#endif //CV_09_EMPLOYEE_H
