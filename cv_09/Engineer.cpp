//
// Created by xmwansa on 26.11.2019.
//

#include "Engineer.h"


float Engineer::getSalary(int years, int education) {

    const int baseSalary = 30000;
    const  int yearBonus = 1000;

    return  baseSalary + years * yearBonus;
}

int Engineer::getRestHoliday(int usedHoliday) {
    const int maxHolidays = 30;

    return maxHolidays - usedHoliday;

}