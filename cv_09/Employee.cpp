//
// Created by xmwansa on 26.11.2019.
//

#include <iostream>
#include "Employee.h"


Employee::Employee(int years, int education, PositionType positionType) {
    m_education = education;
    m_years = years;
    m_usedHoliday = 0;
    m_position = nullptr;
    changeJobPosition(positionType);

}

void Employee::changeJobPosition(PositionType newPosition) {
    if(m_position != nullptr){
        delete  m_position;
    }
    if(newPosition == PositionType::ACADEMIC){
        m_position = new Academic();
    }
    else {
        m_position = new Engineer();
    }
}
void Employee::addNewHoliday(int newHoliday) {
    m_usedHoliday += newHoliday;
}
void Employee::printInfo() {
    cout << "Zbytek dovoleny: "<< m_position->getRestHoliday(m_usedHoliday) << endl;
    cout << "Plat: " << m_position->getSalary(m_years, m_education) << endl;
}