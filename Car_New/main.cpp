#include <iostream>
using namespace std;

class Auto {
public:
    string m_spz;
    int m_rychlost = 50;
    bool m_pohyb;

    void SetSpz(string newSpz){
        m_spz = newSpz;
    }

    string GetSpz(){
        return m_spz;
    }
    void SetPohyb(bool newPohyb){
        m_pohyb = newPohyb;
    }

    void printInfo(){
        if (m_pohyb == true){

            cout << "Auto " << GetSpz() << "jede " << m_rychlost <<"Km/h";
        }
        else{
            cout << "Auto nejede";
        }
        cout << endl;

    }

};


int main() {

    Auto *ferrari = new Auto();
    ferrari->SetSpz("Brno123 ");
    ferrari->printInfo();
    ferrari->SetPohyb(true);
    ferrari->printInfo();
    ferrari->SetPohyb(false);
    ferrari->printInfo();

    return 0;
}